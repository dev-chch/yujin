/**
 * @author https://www.webeenknew.net/
 */

$(document).ready(function () {
	setTimeout(function () {
		var displayBody = setInterval(function () {
			var bodyStyle = $("body").css("display");

			if (bodyStyle === "none") {
				$('body').fadeIn('slow');
			} else {
				clearInterval(displayBody);
			}
		}, 100);
	});

	$('a').click(function (event) {
		if ($(this).hasClass('nohref')) {
			return true;
		}

		event.preventDefault(); // 기본 링크 이벤트를 막음

		$('body').fadeOut('slow');

		var target = $(this).attr('href');

		window.location.href = target;
	});

	$('body').fadeIn('slow');

	if (typeof (AOS) != 'undefined') {
		AOS.init();
	}

	$(".menu-button ").click(function () {
		$("#menu").toggleClass("-translate-y-full transform translate-y-0");
	});
});