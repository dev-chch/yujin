/** @type {import('tailwindcss').Config} */
module.exports = {
	important: true,
	content: [
		"./*.html",
		"./page/*.html",
		"./creativity/*.html",
	],
	theme: {
		screens: {
			'2xl': {'max': '1535px'},
			'xl': {'max': '1279px'},
			'lg': {'max': '1023px'},
			'mw': {'max': '768px'},
			'sm': {'max': '639px'},
		},
	},
	plugins: [],
}
